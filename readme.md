# Tesselar Solutions Architect

![This is an image](./assets/img/tesselar-logo.jpg)

## Misión del puesto

Transformar requerimientos funcionales y lógica de negocios en arquitectura,  requerimientos técnicos y estimación que sean la base para el desarrollo de las soluciones de software que ofrece Tesselar

## Responsabilidades

* Validar la viabilidad técnica minimizando los riesgos
* Definir estándares de calidad en desarrollo de software, así como dar seguimiento de la ejecución de los mismos
* Dimensionar el tiempo y alcance técnico inicial
* Definir las tecnologías a implementar
* Crear arquitectura base y análisis técnico en conjunto con el equipo de implementación y preventa
* Apoyar en las diferentes actividades que requiera alta especialización en el uso de tecnologías web y de desarrollo de software para el equipo de Development and Innovation
* Validar el desarrollo del producto con base en la arquitectura propuesta inicial

[Flujo de trabajo](./work-flows/general.md)