# Descripción de actividades

1. Detección de necesidades. Proceso de identificar aquellas situaciones en las que Tesselar pueda ofrecer un producto o servicio de software que resuelvan problemas o mejoren los procesos de los clientes
    * Entregable: …
    * Responsables: Analista de negocio
 
1. Planteamiento del problema. Se refiere en aterrizar qué es lo que debe hacer nuestro producto o servicio para satisfacer una o más de las necesidades detectadas en el punto 1.
    * Entregable: …
    * Responsables: Analista de negocio
 
1. Toma de requerimientos. A partir de una sesión con el cliente se obtiene específicamente que es aquello que requiere
    * Entregable: formato de brief con los requerimientos del cliente (de primera mano)
    * Responsables: Analista de negocio
 
1. Análisis general del procesºo
    * Entregables: Diagrama de flujo de proceso general y definición del software
    * Responsables: Analista de negocio
 
1. Definición de reglas de negocio. Listado de restricciones, condiciones y aseveraciones cons las que debe cumplir el software
    * Entregables: Listado de reglas de negocio
    * Responsables: Analista de negocio

1. Dimensionamiento del proyecto.
    * Entregables: documento que contiene el tiempo aproximada que tomará crear la solución
    * Responsable: Arquitecto de solución y facilitador @todo Valida

1. Validación con cliente
    * Entregables: firma de cliente para análisis del proceso
    * Responsables: Analista de negocio y dueño de producto @todo antes del dimensionamiento
 
1. Creación de flujos de subprocesos y arquitectura de la información @todo diseño participa con analista de negocio
    * Entregables: Diagramas de flujo y de caso de uso, listado de actores de la aplicación, modularización de la aplicación
    * Responsables: Arquitecto de solución y diseñador de interfaces y experiencia de usuario
 
 @todo Antes de presentar con cliente se deberá validar con AS

1. Arquitectura de base de datos, arquitectura de software, diseño de UI. Definir estructura de base de datos, tipo de software a crear (monolítico o servicios), definición de API’s, librerías, sdk’s  o frameworks que apliquen (para crear y para utilizar, siendo estos últimos sugeridos) que apliquen. En este punto se deben crear aquellos componentes visuales (mockup o prototipo) que servirán de guía para el desarrollo
    * Entregables: Diagrama de base de datos, diseño de API, arquitectura de componentes (UI), mockups o prototipos visuales
    * Responsables: Arquitecto de solución y diseñador de interfaces y experiencia de usuario
 
1. Creación de actividades de codificación (user stories, casos de uso, etc). En este punto se definen las tareas a realizar de forma específica, tal como pueden ser endpoints de un web service, métodos, componentes o módulos, aquí es donde se realiza la estimación del producto.
    * Entregables: Listado de actividades a realizar como developer
    * Responsables: Arquitecto de solución y desarrolladores

    @todo revisar dimensionamiento con las demás áreas
 
1. Validación de actividades a codificar y asignación. El facilitador valida el material para codificar y asigna a su equipo con base en el expertise de cada miembro
    * Entregables: Aceptación de actividades para codificar y asignación
    * Responsable: Facilitador
 
1. Codificación. La parte más divertida de todas. Crear piezas de software desde 0, resolver problemas de manera eficaz y limpia a través de lenguajes informáticos.
    * Entregables: Código funcional, pruebas automatizadas, documentación generada de forma automática, etc.
    * Responsables: Facilitador y desarrolladores
    * Material necesario:
        * Snippets de código
        * Guías de estilo de codificación (depende del lenguaje)
        * Linter configurado con guía de estilo de codificación
        * Arquitectura de software definida (estructura de archivos, clases, paquetes, objetos...)
        * Flujo de trabajo automatizado (visual studio, grunt, webpack, gulp, ...)
 
1. Aseguramiento de la calidad. Revisión de código, ejecución de pruebas automatizadas, revisión a lineamientos de marca y diseño
    * Entregables: Evidencia de ejecución de pruebas
    * Responsables: Facilitador, desarrolladores y arquitecto de solución, diseño
    * Material necesario:
        * Suite de pruebas automatizadas
        * Linter para revisión de estilo de codificación

1. Integración. A partir del código generado, se deberán crear las versiones, branches correspondientes para mantener un orden en las versiones.
    * Entregables: módulos de software funcionales listos para utilizarse
    * Responsables: Facilitador y desarrolladores
 
1. Publicación. Publicación de software funcional en los entornos correspondientes para su uso
    * Entregables: Software funcional en productivo
    * Responsables: Facilitador y arquitecto de solución

1. Generar manual de funcionamiento (nivel usuario)
    * Entregables: manual de uso del software
    * Responsables: ? [Propuesta: Technical writer]
 
A partir del punto 7, el facilitador deberá mantenerse informado del estatus del proyecto, a partir del punto 9, se encargará de asegurarse que las actividades se dan en tiempo y forma (seguimiento).