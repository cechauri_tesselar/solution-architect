# Talentry

## Actividades

* Predimensionamiento @todo ?
  * Entregables
    * Documento que contiene:
      * Alcance en fechas
      * Funcionalidad a implementar *** @todo

* Planeación del proyecto
  * Tener una sesión para verificar disponibilidad en calendario con equipo de desarrollo

* Análisis de la información
  * Entregables
    * Listado de actores
    * Diagramas de caso de uso
    * Diagramas de flujo
    * Reglas de negocio

* Validación con cliente

* @todo Diseño de UI/UX

* Análisis técnico
  * Definición de tecnologías a utilizar (frameworks, sdk's, apis, librerías)

* Diseño de software
  * Entregables
    * Arquitectura de componentes (angular js)
    * Arquitectura de API

* Definición de actividades de desarrollo
  * Entregables
    * User stories

* Validación de actividades de desarrollo
  * Validación por parte de facilitador de desarrollo de software

* Validación con cliente
  * Validación de funcionalidades y visualización

* Pase a desarrollo
  * Sesión para explicar el material generado

* Validación de proyecto
  * Una vez finalizado el proyecto revisión de funcionalidades

* Presentación de proyecto a cliente
  * Se realiza una presentación con cliente de las funcionalidades desarrolladas
  * Entrega de documentación técnica
  * Entrega de guía de uso
  * Entrega de guía de soporte

* Capacitación en el uso del producto
  * {definir por proyecto}

Publicación