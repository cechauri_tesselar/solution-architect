# Flujo de trabajo general para proyectos de software Tesselar

## Participantes

* Consultor / Ingeniero de preventa
* UX / UI Designer
* Solution Architect
* Facilitador de desarrollo de software
* Desarrollador
* Ingeniero de soporte

Las diferentes actividades, así como las responsabilidad de ejecutar cada una de ellas se puede consultar en el siguiente [documento](../docs/general-workflow.xlsx) situado en la raíz de este proyecto: ```./docs/general-workflow.xlsx``` y en este [diagrama de flujo](https://www.draw.io/?state=%7B%22ids%22:%5B%221dcd_jLiL1x4oqOU3Q_rTjnQ2pqnczdrg%22%5D,%22action%22:%22open%22,%22userId%22:%22112154732068212309611%22%7D#G1dcd_jLiL1x4oqOU3Q_rTjnQ2pqnczdrg)

A continuación se describe cada una de las actividades:

* Detección de necesidades: Se realiza un diagnóstico con el cliente para verificar qué necesita más allá de qué es lo que pide

* Solicitud: El consultor da de alta un proyecto (@todo verificar donde)

* Levantamiento de información: Se recaba toda la información necesaria del cliente, puede incluir la siguiente información
  * Procesos
  * Lógica de negocio
  * Reglas de negocio

* Prediomensionamiento + 20%: Con base en la información obtenida del cliente se realiza un predimensionamiento de forma general para así poder dar fechas al cliente, debe **incluir el alcance**

* Planeación del proyecto: Se verifica disponibilidad del equipo de desarrollo

* Análisis de la información: Se analizan los procesos de forma detallada, creando esquemas de información, puede incluir:
  * Diagramas de flujo general
  * Diagramas de caso de uso
  * Diagrama de clases

* Análisis técnico (Infraestructura) - En esta actividad se definen las tecnologías a utilizar:
  * Lenguajes de programación
  * Frameworks
  * Plataformas

* Análisis de proceso - Se crean diagramas flujo específicos por proceso, es posible que se requiera regresar a añadir aspectos o modificar lo creado en el análisis de la información

* Diseño de interfaces - Se crean interfaces de alta fidelidad

* Diseño de software - Se crea la arquitectura y componentes de software esqueleto que se requieren para el proyecto

* Definición de actividades de desarrollo - Se enlistan y estiman aquellas actividades que se requieren de desarrollo para poder completar el proyecto (@todo especificar qué se requiere)

* Validación de actividades de desarrollo y producto (precheck) - Se realiza una validación del tiempo y definición de las actividades a crear por desarrollo

* Validación con cliente - Se valida con el cliente el funcionamiento del producto a desarrollar

* Pase a desarrollo - Se realiza el pase del proyecto a desarrollo, donde se explican las actividades y el material de documentación generado al momento

* Ejecución de actividades (codificación) - Se da inicio a la codificación/configuración del producto

* Revisión de código - Como parte de las actividades de calidad, se valida el código que cuente con el apego a estándares

* Pruebas - Se comprueba el funcionamiento del producto

* Validación del proyecto - Se realiza una validación a nivel general del producto

* Presentación de proyecto con cliente - Se presenta el producto final con cliente

* Generación de material básico de uso - Creación de guías para uso del sistema

* Pase a productivo (publicación) - Se publica el proyecto en el entorno de producci´n

* Acompañamiento - Consultoría al cliente en el uso del producto creado

* Garantía - En esta actividad se corrigen errores que aparezcan en producción

* Soporte y mantenimiento - Se da seguimiento al proyecto asegúrandose de que se mantiene funcionando y se resuelven dudas acerca del mismo